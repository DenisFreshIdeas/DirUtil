﻿using NUnit.Framework;
using DirUtil.Services.FileSystemWalkers;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Reflection;
using NUnit.Framework.Internal;
using DirUtil.Models.Consts;

namespace DireUtil.UnitTests
{
	[TestFixture]
	public class OSWalkersTests
	{
		private IFileSystemWalker osWalker;
		private string rootPath;
		private int amountTestFiles;

		private List<string> filesNamesInTestDirectory;

		[SetUp]
		public void Initialize()
		{

			var currentDirectoryPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

			rootPath = Path.Combine(Directory.GetParent(currentDirectoryPath).Parent.FullName, "root");

			filesNamesInTestDirectory = new List<string>
			{
				"root1File.txt",
				"root2File.txt",
				"root3File.txt",
				"rootSourceFile.cpp",
				"main1File.txt",
				"main1SourceFile.cpp",
				"main2File.txt",
				"main3File.txt",
				"file_source1.cpp",
				"file_source10.cpp",
				"file_source2.cpp",
				"file_source3.cpp",
				"file_source4.cpp",
				"file_source5.cpp",
				"file_source6.cpp",
				"file_source7.cpp",
				"file_source8.cpp",
				"file_source9.cpp",
				"main1File.txt",
				"main2File.txt",
				"main3File.txt",
				"main3Source.cpp",
				"main1File.txt",
				"main1Source.cpp",
				"main2File.txt",
				"main3File.txt"
			};

			amountTestFiles = filesNamesInTestDirectory.Count;

			filesNamesInTestDirectory.Sort();


			if (System.Environment.OSVersion.Platform == System.PlatformID.Win32NT)
			{
				osWalker = new WindowsFileWalker();
			}
			else
			{
				// For Future Use (Linux, MacOS)
			}
		}



		[Test]
		public async Task TestWalkInDirectories()
		{
			List<string> discoveredFiles = new List<string>();

			await osWalker.WalkInRootFolderAsync(rootPath, async (filesPaths) =>
			{

				foreach (var filePath in filesPaths)
				{
					discoveredFiles.Add(Path.GetFileName(filePath));
				}

				await Task.Yield();
			});

			discoveredFiles.Sort();

			Assert.AreEqual(amountTestFiles, discoveredFiles.Count);
			CollectionAssert.AreEqual(filesNamesInTestDirectory, discoveredFiles);
		}

		[Test]
		public async Task TestCppWalkInDirectories()
		{
			List<string> discoveredFiles = new List<string>();
			List<string> cppFilesInTestDirectory = filesNamesInTestDirectory.FindAll((file) => { return file.EndsWith("cpp"); });

			await osWalker.WalkInRootFolderAsync(rootPath, async (filesPaths) =>
			{

				foreach (var filePath in filesPaths)
				{
					discoveredFiles.Add(Path.GetFileName(filePath));
				}

				await Task.Yield();
			}, PatternsOfFilesSearch.CPP);


			Assert.AreEqual(cppFilesInTestDirectory.Count, discoveredFiles.Count);
			CollectionAssert.AreEquivalent(cppFilesInTestDirectory, discoveredFiles);
		}
	}
}
