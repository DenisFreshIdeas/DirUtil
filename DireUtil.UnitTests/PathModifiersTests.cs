﻿using NUnit.Framework;
using DirUtil.Helpers.FilePathModifierHelpers;
using NUnit.Framework.Internal;

namespace DireUtil.UnitTests
{
	[TestFixture(Description = "It is a class for testing path modifiers classes")]
	public class PathTests
	{
		private string testPath = "C:\\root\\test\\course.cpp";
		private IPathModifier pathModifier;

		[SetUp]
		public void InitialSetUp()
		{
			pathModifier = new DefaultPathModifier();
		}

		[Test, Order(1), Description("Test of DefaultPathModifier class")]
		public void TestDefaultPathModifier()
		{
			var defaultPath = string.Format("{0}{1}{2}", "   ", testPath, "    ");

			Assert.AreEqual(testPath, pathModifier.ModifyFilePath(defaultPath));
		}

		[Test, Order(2), Description("Test of CppFilePathModifier class")]
		public void TestCppPathModifier()
		{
			pathModifier = new CppFilePathModifier();

			Assert.AreEqual("C:\\root\\test\\course.cpp /", pathModifier.ModifyFilePath(testPath));
		}

		[Test, Order(3), Description("Test of ReveresedFilePathModifier class")]
		public void TestReveresedFilePathModifier()
		{
			pathModifier = new ReveresedFilePathModifier();

			Assert.AreEqual("course.cpp\\test\\root\\C:", pathModifier.ModifyFilePath(testPath));
		}

		[Test, Order(4), Description("Test of ReveresedFilePathOrderModifier class")]
		public void TestReveresedFilePathOrderModifier()
		{
			pathModifier = new ReveresedFilePathOrderModifier();

			Assert.AreEqual("ppc.esruoc\\tset\\toor\\:C", pathModifier.ModifyFilePath(testPath));
		}
	}
}
