﻿using Autofac;
using DirUtil.Configuration.Modules;

namespace DirUtil.Configuration
{
	/// <summary>
	/// Class that represents wrapper over the Autofac.IContainer type.
	/// Class protects container state and perfoms first initialization logic of Autofac.IContainer type.
	/// 
	/// <see cref="IContainer"/>
	/// </summary>
	public static class DIContainer
	{
		public static IContainer Container { get; private set; }


		public static void InitializeContainerBuilder()
		{
			if (Container == null)
			{
				var builder = new ContainerBuilder();

				builder.RegisterModule(new PathModifiersModule());
				builder.RegisterModule(new OSFileWalkerModule());
				builder.RegisterModule(new ExecutorsModule());
				builder.RegisterModule(new CreatorsCommandsModule());

				Container = builder.Build();
			}
		}
	}
}
