﻿using Autofac;
using DirUtil.Helpers.FilePathModifierHelpers;
using DirUtil.Models.Consts;

namespace DirUtil.Configuration.Modules
{
	/// <summary>
	/// Class that represents initialization module for register dependencies  type of <see cref="IPathModifier"/> interface.
	/// 
	/// </summary>
	public class PathModifiersModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.Register(context => new CppFilePathModifier()).Named<IPathModifier>(Actions.CPP);
			builder.Register(context => new ReveresedFilePathModifier()).Named<IPathModifier>(Actions.REVERESED1);
			builder.Register(context => new ReveresedFilePathOrderModifier()).Named<IPathModifier>(Actions.REVERESED2);
			builder.Register(context => new DefaultPathModifier()).Named<IPathModifier>(Actions.ALL);
		}
	}
}
