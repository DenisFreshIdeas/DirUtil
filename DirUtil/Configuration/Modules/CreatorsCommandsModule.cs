﻿using Autofac;
using DirUtil.CommandsServices.CommandsCreatorsServices;
using DirUtil.Models.Consts;


namespace DirUtil.Configuration.Modules
{
	/// <summary>
	///  Class that represents initialization module for register dependencies  <see cref="ICommandCreator"/> interface.
	/// 
	/// </summary>
	public class CreatorsCommandsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{

			builder.Register(context => new SearhCommandCreator()).Named<ICommandCreator>(CommandCreators.SEARCH_COMMAND_CREATOR);
		}
	}
}
