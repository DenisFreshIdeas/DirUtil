﻿using Autofac;
using DirUtil.Services.FileSystemWalkers;
using DirUtil.Models.Consts;
using DirUtil.Helpers.FilePathModifierHelpers;
using DirUtil.CommandsServices.CommandsExecutorsServices;

namespace DirUtil.Configuration.Modules
{
	/// <summary>
	/// Class that represents initialization module for register dependencies type of <see cref="ICommandExecutor"/> interface.
	/// 
	/// </summary>
	public class ExecutorsModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.Register<CommandExecutor>((context, paramater) =>
			{
				var executorType = paramater.Named<string>("executorType");
				var actionName = paramater.Named<string>("actionName");


				if (executorType == Executors.SEARCH_EXECUTOR)
				{
					SearchCommandExecutor searchExecutor = new SearchCommandExecutor(context.Resolve<IFileSystemWalker>());

					searchExecutor.PathModifier = context.ResolveNamed<IPathModifier>(actionName);

					return searchExecutor;
				}
				else
				{
					SearchCommandExecutor searchExecutor = new SearchCommandExecutor(context.Resolve<IFileSystemWalker>());
					searchExecutor.PathModifier = context.ResolveNamed<IPathModifier>("actionName");

					return searchExecutor;
				}
			}).As<ICommandExecutor>();

		}
	}
}
