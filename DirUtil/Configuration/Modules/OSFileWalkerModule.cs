﻿using System;
using Autofac;
using DirUtil.Services.FileSystemWalkers;

namespace DirUtil.Configuration.Modules
{
	/// <summary>
	/// Class that represents initialization module for register dependencies type of <see cref="IFileSystemWalker"/>.
	/// Object of IFileSystemWalker type depends from the current execution platform (Windows, Linux(in future), MacOs(in future)). 
	/// 
	/// </summary>
	public class OSFileWalkerModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			if (Environment.OSVersion.Platform == PlatformID.Win32NT)
			{
				builder.Register(context => new WindowsFileWalker()).As<IFileSystemWalker>();
			}

			// Also can be another platforms in feature
		}
	}
}
