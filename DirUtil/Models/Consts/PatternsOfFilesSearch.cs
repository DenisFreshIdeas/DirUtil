﻿
namespace DirUtil.Models.Consts
{
	/// <summary>
	/// Class that contains all available search patterns of file actions <see cref="Models.Actions.FileAction"/>
	/// 
	/// </summary>
	public static class PatternsOfFilesSearch
	{
		public const string ALL = "*";
		public const string CPP = "*.cpp";
	}
}
