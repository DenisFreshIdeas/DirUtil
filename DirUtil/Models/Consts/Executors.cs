﻿
namespace DirUtil.Models.Consts
{
	/// <summary>
	/// Class that contains all names of command executors <see cref="Services.AppManagers"/>
	/// 
	/// </summary>
	public static class Executors
	{
		public const string SEARCH_EXECUTOR = "Search";
	}
}
