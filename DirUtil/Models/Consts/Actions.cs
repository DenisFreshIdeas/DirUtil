﻿
namespace DirUtil.Models.Consts
{
	/// <summary>
	/// Class that contains all available actions of commands <see cref="Commands.Command"/>
	/// 
	/// </summary>
	/// 
	public static class Actions
	{
		public const string ALL = "all";
		public const string CPP = "cpp";
		public const string REVERESED1 = "reveresed1";
		public const string REVERESED2 = "reveresed2";
	}
}
