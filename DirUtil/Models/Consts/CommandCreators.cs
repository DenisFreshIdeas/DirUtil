﻿
namespace DirUtil.Models.Consts
{
	/// <summary>
	/// Class that contains all names of commands creators <see cref="Helpers.CommandsHelpers.CommandCreatorsHelpers"/>
	/// 
	/// </summary>
	public static class CommandCreators
	{
		public const string SEARCH_COMMAND_CREATOR = "SearchCreator";
	}
}
