﻿
namespace DirUtil.Models.Actions
{
	/// <summary>
	/// Class that represents common abstraction of file path actions
	/// 
	/// </summary>
	public abstract class FileAction
	{
		public ActionType ActionType { get; protected set; }
		public string SearchPattern { get; protected set; }
		public string ActionName { get; protected set; }
	}
}
