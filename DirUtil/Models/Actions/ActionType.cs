﻿
namespace DirUtil.Models.Actions
{
	/// <summary>
	/// Enum that represents type of action that contains command <see cref="Commands.Command"/>
	/// 
	/// !Note, DELETE, COPY for future use
	/// </summary>
	public enum ActionType
	{
		SEARCH,
		DELETE,
		COPY,
		UNDEFINED
	}
}
