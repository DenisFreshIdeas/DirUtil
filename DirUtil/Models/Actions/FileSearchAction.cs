﻿using System;

namespace DirUtil.Models.Actions.SearchActions
{
	/// <summary>
	/// Class that represents model of action for searching files in directories
	/// 
	/// </summary>
	public class FileSearchAction : FileAction
	{

		public FileSearchAction(string searchPattern, string actionName)
		{
			if (searchPattern == null)
				throw new ArgumentNullException(nameof(searchPattern), "Argument can not be null");

			if (actionName == null)
				throw new ArgumentNullException(nameof(actionName), "Argument can not be null");

			ActionType = ActionType.SEARCH;
			ActionName = actionName;
			SearchPattern = searchPattern.Trim();
		}
	}
}
