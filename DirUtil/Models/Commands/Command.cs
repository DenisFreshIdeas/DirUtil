﻿using DirUtil.Models.Actions;

namespace DirUtil.Models.Commands
{
	/// <summary>
	/// Class that represents abstraction of console command that can execute some actions
	/// and contains another arguments required for command execution
	/// 
	/// </summary>
	public class Command
	{
		public string RootFolder { get; set; }
		public FileAction CommandAction { get; set; }
		public string RootLoggerDirectory { get; set; }
	}
}
