﻿using System;
using System.Threading.Tasks;
using DirUtil.Configuration;
using DirUtil.Services.AppManagers;

namespace DirUtil
{
	class MainClass
	{
		private static AppConsoleCommandManager consoleInputManager = new AppConsoleCommandManager();

		public static void Main(string[] args)
		{
			Task.Run(async () =>
			{
				DIContainer.InitializeContainerBuilder();

				try
				{
					var executionResult = await consoleInputManager.ProcessInput(args);

					if (executionResult == true)
					{
						Console.WriteLine("Command successfully finished");
					}
					else
					{
						Console.WriteLine("Command is not valid, please try again");
					}
				}
				catch (Exception exc)
				{
					Console.WriteLine("Command execution failed, please try again");
				}

			}).Wait();
		}
	}
}
