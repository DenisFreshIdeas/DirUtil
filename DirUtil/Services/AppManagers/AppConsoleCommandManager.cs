﻿using System.Threading.Tasks;
using DirUtil.Models.Commands;
using DirUtil.Helpers.Validators.CommandsValidators;
using DirUtil.Helpers.ActionHelpers;
using DirUtil.Configuration;
using DirUtil.CommandsServices.CommandsCreatorsServices;
using DirUtil.CommandsServices.CommandsExecutorsServices;

namespace DirUtil.Services.AppManagers
{
	/// <summary>
	/// Class that represents manager for processing console input
	/// 
	/// </summary>
	public class AppConsoleCommandManager
	{
		private CommandCreatorManager _commandCreatorManager = new CommandCreatorManager();
		private CommandsExecutorsManager _commandsExecutorsManager = new CommandsExecutorsManager();
		private AvailableActionsHelper _availableActionsHelper = new AvailableActionsHelper();

		public async Task<bool> ProcessInput(string[] args)
		{
			if (args == null || args.Length < 2)
				return false;

			using (var scope = DIContainer.Container.BeginLifetimeScope())
			{
				bool executionResult = false;

				ICommandCreator commandCreator = _commandCreatorManager.InitializeCommandCreator(_availableActionsHelper.ActionTypeByName(args[1]));

				if (commandCreator != null)
				{
					Command command = commandCreator.CreateCommand(args);

					if (command != null)
					{
						ICommandExecutor commandExecutor = _commandsExecutorsManager.GetCommandExecutor(command);

						if (commandExecutor != null)
						{
							await commandExecutor.ExecuteCommand(command, new SimpleCommandsValidator());

							executionResult = true;
						}
					}
				}


				return executionResult;
			}
		}
	}
}
