﻿using System.Threading.Tasks;

namespace DirUtil.Services.Loggers
{
	public interface ILogger
	{
		Task<bool> WriteAsync(string[] value, string filePath, bool appendResult = true);
	}
}
