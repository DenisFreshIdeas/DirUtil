﻿using System;
using System.Threading.Tasks;
using System.IO;
using DirUtil.Helpers.Validators.PathValidators;

namespace DirUtil.Services.Loggers
{
	public class FileLogger : ILogger
	{
		private const string FILE_NAME = "results.txt";

		public async Task<bool> WriteAsync(string[] value, string filePath, bool appendResult = true)
		{
			if (value == null)
				throw new ArgumentNullException(nameof(value), "Argument can not be null");

			if (filePath == null)
				throw new ArgumentNullException(nameof(filePath), "Argument can not be null");

			bool result = false;

			try
			{

				if (PathValidator.IsDirectoryExists(filePath))
				{
					string patternString = null;

					if (!filePath.EndsWith("\\"))
						patternString = "{0}\\{1}";
					else
						patternString = "{0}{1}";

					filePath = string.Format(patternString, filePath, FILE_NAME);

				}

				if (Directory.Exists(Path.GetDirectoryName(filePath)))
				{
					lock (filePath)
					{
						using (var writer = new StreamWriter(filePath, appendResult))
						{
							for (int i = 0; i < value.Length; i++)
							{
								writer.WriteLineAsync(value[i]);
							}

						}

						result = true;
					}
				}
			}
			catch (UnauthorizedAccessException unauthorizedAccessException)
			{

			}
			catch (IOException exception)
			{

			}
			catch (Exception exc)
			{

			}

			await Task.Yield();

			return result;
		}
	}
}
