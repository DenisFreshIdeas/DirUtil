﻿using System.Threading.Tasks;
using System;

namespace DirUtil.Services.FileSystemWalkers
{
	/// <summary>
	/// An interface that represents behaviour for walking in directories of os systems
	/// 
	/// </summary>
	public interface IFileSystemWalker
	{
		/// <summary>
		/// Walks in all subdirectories of a specified root directory
		/// </summary>
		/// <param name="rootDirectoryPath">Root directory path.</param>
		/// <param name="filesHandler">Files paths handler.</param>
		/// <param name="searchPattern">File search pattern by extension.</param>
		Task WalkInRootFolderAsync(string rootDirectoryPath, Func<string[], Task> filesHandler, string searchPattern = "*");
	}
}
