﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;

namespace DirUtil.Services.FileSystemWalkers
{
	/// <summary>
	/// Class that represents "os walker" that walking in directories of Windows file system (NTFS)
	/// 
	/// </summary>
	public class WindowsFileWalker : IFileSystemWalker
	{
		public async Task WalkInRootFolderAsync(string rootDirectoryPath, Func<string[], Task> filesHandler, string searchPattern = "*")
		{
			await Task.Run(() => { WalkInRootFolder(rootDirectoryPath, filesHandler, searchPattern); }).ConfigureAwait(false);
		}

		private void WalkInRootFolder(string rootDirectoryPath, Func<string[], Task> filesHandler, string searchPattern)
		{
			if (rootDirectoryPath == null)
				throw new ArgumentNullException(nameof(rootDirectoryPath), "Argument can not be null");

			try
			{
				Stack<string> directories = new Stack<string>(20);

				if (Directory.Exists(rootDirectoryPath))
				{
					directories.Push(rootDirectoryPath);

					while (directories.Count > 0)
					{
						try
						{
							string currentDirectory = directories.Pop();

							if (Directory.Exists(currentDirectory))
							{
								string[] subDirectories = Directory.GetDirectories(currentDirectory);
								string[] currentFilesPaths = Directory.GetFiles(currentDirectory, searchPattern);

								if (currentFilesPaths != null && currentFilesPaths.Length > 0)
								{
									filesHandler?.Invoke(currentFilesPaths);
								}


								for (int i = 0; i < subDirectories.Length; i++)
								{
									directories.Push(subDirectories[i]);
								}
							}
						}
						catch (Exception exc)
						{
							// Suppresing exceptions
							Console.WriteLine(exc.Message);
							continue;
						}
					}
				}
			}
			catch (UnauthorizedAccessException unauthorizedException)
			{
				Console.WriteLine(unauthorizedException.Message);
			}
			catch (DirectoryNotFoundException notFoundException)
			{
				Console.WriteLine(notFoundException.Message);
			}
		}
	}
}
