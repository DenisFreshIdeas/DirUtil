﻿using DirUtil.Models.Commands;

namespace DirUtil.CommandsServices.CommandsCreatorsServices
{
	/// <summary>
	/// Interface that represents behaviour for creating objects type of <see cref="Command"/>
	/// </summary>
	public interface ICommandCreator
	{
		/// <summary>
		/// Method for creating objects type of <see cref="Command"/> based on input arguments values
		/// </summary>
		/// 
		/// <returns>The concrete commmand</returns>
		/// <param name="args">Console arguments</param>
		Command CreateCommand(string[] args);
	}
}
