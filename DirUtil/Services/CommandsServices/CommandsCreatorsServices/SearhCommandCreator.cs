﻿using DirUtil.Models.Commands;
using DirUtil.Helpers.ActionHelpers;
using System.IO;
using DirUtil.Helpers.ActionsHelpers;
using DirUtil.Helpers.Validators.PathValidators;
using DirUtil.Models.Actions;
using DirUtil.Models.Actions.SearchActions;

namespace DirUtil.CommandsServices.CommandsCreatorsServices
{
	/// <summary>
	/// Class that represents service for creation search commands 
	/// (Objects type of <see cref="Command"/> that have action type of <see cref="ActionType.SEARCH"/> )
	/// 
	/// </summary>
	public class SearhCommandCreator : ICommandCreator
	{
		protected AvailableActionsHelper _actionsHelper;
		protected SearchPatternHelper _patternHelper;

		public SearhCommandCreator()
		{
			_actionsHelper = new AvailableActionsHelper();
			_patternHelper = new SearchPatternHelper();
		}

		public Command CreateCommand(string[] args)
		{
			if (args == null || args.Length < 3)
				return null;

			string rootFolder = args[0];
			string actionName = args[1];
			string resultFolderPath = args[2];

			if (resultFolderPath != null)
			{
				if (PathValidator.IsDirectoryExists(resultFolderPath))
				{
					if (!resultFolderPath.EndsWith("\\"))
					{
						resultFolderPath = string.Format("{0}{1}", resultFolderPath, "\\");
					}
				}
				else if (Path.GetExtension(resultFolderPath) == string.Empty)
				{
					resultFolderPath = null;
				}
			}

			if (!PathValidator.IsDirectoryExists(rootFolder))
				rootFolder = null;

			Command command = null;

			if (rootFolder != null && actionName != null && resultFolderPath != null)
			{
				if (_actionsHelper.IsActionAvailable(actionName))
				{
					ActionType actionType = _actionsHelper.ActionTypeByName(actionName);

					if (actionType != ActionType.UNDEFINED)
					{
						string searchPattern = _patternHelper.GetSearchPatternByActionName(actionName);

						if (searchPattern != null)
						{
							FileAction action = new FileSearchAction(searchPattern, actionName);

							command = new Command { RootFolder = Path.GetFullPath(rootFolder), CommandAction = action, RootLoggerDirectory = Path.GetFullPath(resultFolderPath) };
						}
					}
				}
			}

			return command;
		}
	}
}
