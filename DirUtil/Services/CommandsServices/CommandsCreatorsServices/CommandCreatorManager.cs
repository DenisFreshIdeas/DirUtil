﻿using DirUtil.Models.Actions;
using DirUtil.Configuration;
using Autofac;
using DirUtil.Helpers.CommandsHelpers.CommandCreatorsHelpers;

namespace DirUtil.CommandsServices.CommandsCreatorsServices
{
	/// <summary>
	/// Class that represents service manager for getting objects type of <see cref="ICommandCreator"/>.
	/// This class hides logic for creation ICommandCreator type objects and helps 
	/// to get required creator for <see cref="Models.Commands.Command"/> type by ActionType (<see cref="ActionType"/>).
	/// 
	/// </summary>
	public class CommandCreatorManager
	{
		protected CommandCreatorTypeHelper _commandCreatorTypeHelper = new CommandCreatorTypeHelper();

		public ICommandCreator InitializeCommandCreator(ActionType actionType)
		{
			ICommandCreator commandCreator = null;

			string commandCreatorType = _commandCreatorTypeHelper.GetCommandCreatorTypeByActionType(actionType);

			if (commandCreatorType != null)
			{
				using (var scope = DIContainer.Container.BeginLifetimeScope())
				{
					commandCreator = scope.ResolveNamed<ICommandCreator>(commandCreatorType);
				}
			}

			return commandCreator;
		}
	}
}
