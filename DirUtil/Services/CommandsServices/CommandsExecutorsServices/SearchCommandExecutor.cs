﻿using System.Threading.Tasks;
using DirUtil.Helpers.FilePathModifierHelpers;
using DirUtil.Services.FileSystemWalkers;

namespace DirUtil.CommandsServices.CommandsExecutorsServices
{
	/// <summary>
	/// Class that provides service for executing search commands 
	/// (Objects type of <see cref="Models.Commands.Command"/> that have action type of <see cref="Models.Actions.ActionType.SEARCH"/> )
	/// 
	/// </summary>
	public class SearchCommandExecutor : CommandExecutor
	{
		protected IPathModifier _pathModifier = new DefaultPathModifier();

		private static object lockerObject = new object();

		public IPathModifier PathModifier
		{

			get
			{
				return _pathModifier;
			}
			set
			{
				if (value != null)
				{
					_pathModifier = value;
				}
			}
		}

		public SearchCommandExecutor(IFileSystemWalker osWalker) : base(osWalker)
		{
		}


		protected override async Task ActionProcessing(string[] filesPaths)
		{
			if (filesPaths != null && filesPaths.Length > 0)
			{
				try
				{
					string[] modifiedFilePaths = new string[filesPaths.Length];

					lock (lockerObject)
					{
						for (int i = 0; i < filesPaths.Length; i++)
						{
							modifiedFilePaths[i] = _pathModifier.ModifyFilePath(filesPaths[i]);
						}
					}

					await _logger.WriteAsync(modifiedFilePaths, CurrentExecutionCommand.RootLoggerDirectory).ConfigureAwait(false);
				}
				catch
				{
					// Suppresign exceptions
				}
			}
		}
	}
}
