﻿using System;
using System.Threading.Tasks;
using DirUtil.Helpers.Validators.CommandsValidators;
using DirUtil.Models.Commands;
using DirUtil.Services.FileSystemWalkers;
using DirUtil.Services.Loggers;

namespace DirUtil.CommandsServices.CommandsExecutorsServices
{
	/// <summary>
	/// Class that represents service for commands execution <see cref="Command"/>
	/// 
	/// </summary>
	public abstract class CommandExecutor : ICommandExecutor
	{
		public Command CurrentExecutionCommand { get; protected set; }

		protected IFileSystemWalker _osWalker;
		protected ILogger _logger = new FileLogger();

		public ILogger Logger
		{
			get
			{
				return _logger;
			}

			set
			{
				if (value != null)
				{
					_logger = value;
				}
			}
		}

		public CommandExecutor(IFileSystemWalker osWalker)
		{
			if (osWalker == null)
				throw new ArgumentNullException(nameof(osWalker), "Argument can not be null");


			_osWalker = osWalker;
		}


		protected abstract Task ActionProcessing(string[] filesPaths);


		public async Task<bool> ExecuteCommand(Command command, ICommandValidator commandValidator)
		{
			if (command == null)
				throw new ArgumentNullException(nameof(command), "Argument can not be null");

			if (commandValidator == null)
				throw new ArgumentNullException(nameof(commandValidator), "Argument can not be null");


			bool statusOfExecution = false;

			try
			{
				if (commandValidator.ValidateCommand(command))
				{
					CurrentExecutionCommand = command;

					await _osWalker.WalkInRootFolderAsync(command.RootFolder, async (filesPaths) =>
					{
						await ActionProcessing(filesPaths);
					}, CurrentExecutionCommand.CommandAction.SearchPattern).ConfigureAwait(false);

					statusOfExecution = true;
				}
			}
			catch (Exception exc)
			{
				Console.WriteLine(exc.Message);
			}
			finally
			{
				CurrentExecutionCommand = null;
			}

			return statusOfExecution;
		}
	}
}
