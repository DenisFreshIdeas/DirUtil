﻿using System.Threading.Tasks;
using DirUtil.Models.Commands;
using DirUtil.Helpers.Validators.CommandsValidators;

namespace DirUtil.CommandsServices.CommandsExecutorsServices
{
	/// <summary>
	/// An interface that represents the behavior for executing commands
	/// 
	/// </summary>
	public interface ICommandExecutor
	{
		Task<bool> ExecuteCommand(Command command, ICommandValidator commandValidator);
	}
}
