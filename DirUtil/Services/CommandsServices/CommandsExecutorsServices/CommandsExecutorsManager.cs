﻿using DirUtil.Configuration;
using DirUtil.Models.Commands;
using Autofac;
using DirUtil.Helpers.CommandsHelpers.CommandsExecutorsHelpers;

namespace DirUtil.CommandsServices.CommandsExecutorsServices
{
	/// <summary>
	/// Class that represents service for creating required executor of command (<see cref="Command"/>) by action type of 
	/// command <see cref="Models.Actions.ActionType"/>
	/// 
	/// </summary>
	public class CommandsExecutorsManager
	{
		protected TypeExecutorHelper _executorTypeHelper;

		public CommandsExecutorsManager()
		{
			_executorTypeHelper = new TypeExecutorHelper();
		}

		/// <summary>
		/// Represents required command executor object type of <see cref="ICommandExecutor"/>
		/// </summary>
		/// 
		/// <returns>Command executor for processing action of the command</returns>
		/// <param name="command">Command that represents abstraction of console command</param>
		public ICommandExecutor GetCommandExecutor(Command command)
		{
			ICommandExecutor commandExecutor = null;

			try
			{
				if (command != null)
				{
					string nameExecutor = _executorTypeHelper.GetExecutorTypeByActionType(command.CommandAction.ActionType);

					if (nameExecutor != null)
					{
						using (var scope = DIContainer.Container.BeginLifetimeScope())
						{
							commandExecutor = scope.Resolve<ICommandExecutor>(new NamedParameter("executorType", nameExecutor),
															new NamedParameter("actionName", command.CommandAction.ActionName));
						}
					}
				}
			}
			catch
			{
				// Suppresing exceptions
			}

			return commandExecutor;
		}
	}
}
