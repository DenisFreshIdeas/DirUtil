﻿using System;
using System.IO;

namespace DirUtil.Helpers.Validators.PathValidators
{
	public static class PathValidator
	{

		public static bool IsDirectoryExists(string directory)
		{
			bool result = false;

			if (directory != null)
			{
				if (Path.IsPathRooted(directory))
				{
					if (Directory.Exists(directory))
						result = true;
				}
			}

			return result;
		}

		public static bool IsFileExists(string filePath)
		{
			bool result = false;

			if (filePath != null)
			{
				if (Path.IsPathRooted(filePath))
				{
					if (File.Exists(filePath))
						result = true;
				}
			}

			return result;
		}
	}
}
