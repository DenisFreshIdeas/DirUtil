﻿using System;
using DirUtil.Models.Commands;
using System.IO;
using DirUtil.Helpers.ActionHelpers;
using DirUtil.Models.Actions;
using DirUtil.Helpers.Validators.PathValidators;

namespace DirUtil.Helpers.Validators.CommandsValidators
{
	public class SimpleCommandsValidator : ICommandValidator
	{
		private AvailableActionsHelper actionsHelper;

		public SimpleCommandsValidator()
		{
			actionsHelper = new AvailableActionsHelper();
		}

		public bool ValidateCommand(Command command)
		{
			if (command == null)
				throw new ArgumentNullException(nameof(command), "Argument can not be null");

			bool commandIsValid = false;

			try
			{
				if (command.RootFolder != null && command.RootLoggerDirectory != null && command.CommandAction != null)
				{
					bool IsFileSaveDirectoryExists = false;

					if (PathValidator.IsDirectoryExists(command.RootLoggerDirectory))
					{
						IsFileSaveDirectoryExists = true;
					}
					else if (Path.GetExtension(command.RootLoggerDirectory) != string.Empty)
					{
						IsFileSaveDirectoryExists = true;
					}


					bool IsRootDirectoryExists = PathValidator.IsDirectoryExists(command.RootFolder);
					bool isActionTypeValid = !(command.CommandAction.ActionType == ActionType.UNDEFINED);

					if (IsRootDirectoryExists && IsFileSaveDirectoryExists && isActionTypeValid)
						commandIsValid = true;

				}
			}
			catch (IOException IOException)
			{
				Console.WriteLine(IOException.Message);
			}

			return commandIsValid;
		}
	}
}
