﻿using DirUtil.Models.Commands;

namespace DirUtil.Helpers.Validators.CommandsValidators
{
	/// <summary>
	/// Interface that represents behaviour for validating command arguments
	/// 
	/// </summary>
	public interface ICommandValidator
	{
		/// <summary>
		///  Method that validates command arguments
		/// </summary>
		/// <param name="command">Abstraction of console command</param>
		bool ValidateCommand(Command command);
	}
}
