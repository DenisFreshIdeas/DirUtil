﻿using System;
using DirUtil.Models.Commands;

namespace DirUtil.Helpers.Validators.CommandsValidators
{
	// Class for future use
	public class CommandsValidatorManager
	{
		public static bool ValidateCommand(Command command, ICommandValidator validator)
		{
			if (validator == null)
				throw new ArgumentNullException(nameof(validator), "Argument can not be null");

			if (command == null)
				throw new ArgumentNullException(nameof(command), "Argument can not be null");

			return validator.ValidateCommand(command);
		}
	}
}
