﻿using System;
using System.Linq;

namespace DirUtil.Helpers.FilePathModifierHelpers
{
	/// <summary>
	/// Class that represents behaviour for inverting entire file path string
	/// </summary>
	public class ReveresedFilePathOrderModifier : IPathModifier
	{
		public string ModifyFilePath(string path)
		{
			if (path == null)
				throw new ArgumentNullException(nameof(path), "Argument can not be null");

			return new string(path.Reverse().ToArray());
		}
	}
}
