﻿using System;
using System.Linq;

namespace DirUtil.Helpers.FilePathModifierHelpers
{
	/// <summary>
	/// Class that represents behaviour for changing path in opposite order
	/// </summary>
	public class ReveresedFilePathModifier : IPathModifier
	{
		public string ModifyFilePath(string path)
		{
			if (path == null)
				throw new ArgumentNullException(nameof(path), "Argument can not be null");

			return string.Join("\\", path.Split('\\').Reverse());
		}
	}
}
