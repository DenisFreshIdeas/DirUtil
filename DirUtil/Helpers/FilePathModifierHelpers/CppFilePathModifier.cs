﻿using System;

namespace DirUtil.Helpers.FilePathModifierHelpers
{
	/// <summary>
	/// Class that represents behaviour for adding '/' symbols at the end of files path
	/// </summary>
	public class CppFilePathModifier : IPathModifier
	{
		public string ModifyFilePath(string path)
		{
			if (path == null)
				throw new ArgumentNullException(nameof(path), "Argument can not be null");

			return path + " /";
		}
	}
}
