﻿
namespace DirUtil.Helpers.FilePathModifierHelpers
{
	/// <summary>
	/// Interface that represents behaviour for path modifying
	/// </summary>
	public interface IPathModifier
	{
		string ModifyFilePath(string path);
	}
}
