﻿using System;

namespace DirUtil.Helpers.FilePathModifierHelpers
{
	/// <summary>
	/// Class that represents behaviour for deleting leading and trailing spaces from the path
	/// </summary>
	public class DefaultPathModifier : IPathModifier
	{
		public string ModifyFilePath(string path)
		{
			if (path == null)
				throw new ArgumentNullException(nameof(path), "Argument can not be null");

			return path.Trim();
		}
	}
}
