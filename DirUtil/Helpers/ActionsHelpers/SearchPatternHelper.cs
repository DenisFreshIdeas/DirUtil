﻿using System.Collections.Generic;
using DirUtil.Models.Consts;

namespace DirUtil.Helpers.ActionsHelpers
{
	/// <summary>
	/// Class that represents helper for getting file search pattern by action name
	/// </summary>
	public class SearchPatternHelper
	{
		private readonly Dictionary<string, string> searchPatternsDictionary = new Dictionary<string, string>();

		public SearchPatternHelper()
		{
			searchPatternsDictionary.Add(Actions.ALL, "*");
			searchPatternsDictionary.Add(Actions.CPP, "*.cpp");
			searchPatternsDictionary.Add(Actions.REVERESED1, "*");
			searchPatternsDictionary.Add(Actions.REVERESED2, "*");
		}

		public string GetSearchPatternByActionName(string actionName)
		{
			if (actionName != null && searchPatternsDictionary.ContainsKey(actionName))
			{
				return searchPatternsDictionary[actionName];
			}

			return null;
		}
	}
}
