﻿using System.Collections.Generic;
using DirUtil.Models.Actions;
using DirUtil.Models.Consts;

namespace DirUtil.Helpers.ActionHelpers
{
	/// <summary>
	/// Class that represents helper for getting action type <see cref="ActionType"/>  by action name.
	/// Also class helps check if the action is exists.
	/// </summary>
	public class AvailableActionsHelper
	{
		private readonly Dictionary<string, ActionType> actionsDictionary = new Dictionary<string, ActionType>();

		public AvailableActionsHelper()
		{
			actionsDictionary.Add(Actions.ALL, ActionType.SEARCH);
			actionsDictionary.Add(Actions.CPP, ActionType.SEARCH);
			actionsDictionary.Add(Actions.REVERESED1, ActionType.SEARCH);
			actionsDictionary.Add(Actions.REVERESED2, ActionType.SEARCH);
		}

		public bool IsActionAvailable(string action)
		{
			bool result = false;

			if (action != null)
				result = actionsDictionary.ContainsKey(action);

			return result;
		}

		public ActionType ActionTypeByName(string action)
		{
			ActionType actionType = ActionType.UNDEFINED;

			if (action != null)
			{
				if (actionsDictionary.ContainsKey(action))
					actionType = actionsDictionary[action];
			}

			return actionType;
		}
	}
}
