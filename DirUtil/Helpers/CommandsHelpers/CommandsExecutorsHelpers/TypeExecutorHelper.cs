﻿using System.Collections.Generic;
using DirUtil.Models.Actions;
using DirUtil.Models.Consts;

namespace DirUtil.Helpers.CommandsHelpers.CommandsExecutorsHelpers
{
	/// <summary>
	/// Class that represents helper for getting executor name by action type (<see cref="ActionType"/>)
	/// of command (<see cref="Models.Commands.Command"/>).
	/// 
	/// </summary>
	public class TypeExecutorHelper
	{
		private readonly Dictionary<ActionType, string> executorsDictionary = new Dictionary<ActionType, string>();

		public TypeExecutorHelper()
		{
			executorsDictionary.Add(ActionType.SEARCH, Executors.SEARCH_EXECUTOR);
		}

		public string GetExecutorTypeByActionType(ActionType actionType)
		{
			string executorName = null;

			if (executorsDictionary.ContainsKey(actionType))
			{
				executorName = executorsDictionary[actionType];
			}


			return executorName;
		}
	}
}
