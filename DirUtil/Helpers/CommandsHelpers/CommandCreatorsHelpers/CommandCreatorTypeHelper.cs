﻿using System.Collections.Generic;
using DirUtil.Models.Actions;
using DirUtil.Models.Consts;

namespace DirUtil.Helpers.CommandsHelpers.CommandCreatorsHelpers
{
	/// <summary>
	///  Class that represents helper for getting command creator type <see cref="CommandCreators"/> by <see cref="ActionType"/> value
	/// 
	/// </summary>
	public class CommandCreatorTypeHelper
	{
		private readonly Dictionary<ActionType, string> commandsCreatorsTypesDictionary = new Dictionary<ActionType, string>();

		public CommandCreatorTypeHelper()
		{
			commandsCreatorsTypesDictionary.Add(ActionType.SEARCH, CommandCreators.SEARCH_COMMAND_CREATOR);
		}

		public string GetCommandCreatorTypeByActionType(ActionType actionType)
		{
			string commandCreatorType = null;

			if (commandsCreatorsTypesDictionary.ContainsKey(actionType))
				commandCreatorType = commandsCreatorsTypesDictionary[actionType];

			return commandCreatorType;
		}
	}
}